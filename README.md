clone this project,

install dependencies:
 npm install



install  mongo db without  auth

install  robo 3t  to see  the data 


to run the app with test environment config:
 npm start


to run for an specific env:
NODE_ENV=iso node app.js

to run  unit tests :
npm run unit-test

to run  functional tests :
npm run iso-test

to run  all tests :
npm run test

to run  eslint:
npm  run eslint


to get the coverage:
npm run coverage



To test the endpoints, install   Postman 


the  port is 3010


| METHOD                  │ ROUTE                                            │
├─────────────────────────┼──────────────────────────────────────────────────┤
│ OPTIONS                 │ *                                                │
├─────────────────────────┼──────────────────────────────────────────────────┤
│ GET                     │ /api/v1/health                                   │
├─────────────────────────┼──────────────────────────────────────────────────┤
│ POST                    │ /api/v1/save-info                                │
└─────────────────────────┴──────────────────────────────────────────────────┘ 

Example  localhost:3010/api/v1/save-info

header     x-access-token : M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d

body  request  example:

{"id":111,"name":"saacsas","lastName":"ssssssasa","email":"sasa","test":"11.111.111-1"}
